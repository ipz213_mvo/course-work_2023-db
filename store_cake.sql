-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 29 2023 г., 17:40
-- Версия сервера: 8.0.30
-- Версия PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `store_cake`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cart`
--

CREATE TABLE `cart` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `product_id` int NOT NULL,
  `size_cart` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `size_cart`, `count`) VALUES
(1, 17, 87, '1kg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int NOT NULL,
  `id_product` int NOT NULL,
  `id_author` int NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int NOT NULL,
  `id_buyer` int NOT NULL,
  `id_products` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_index` int NOT NULL,
  `execute` int DEFAULT NULL,
  `date` datetime NOT NULL,
  `counts` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `id_buyer`, `id_products`, `lastname`, `firstname`, `phone`, `address`, `post_index`, `execute`, `date`, `counts`, `sizes`) VALUES
(35, 17, '52', 'Vlad', 'dsadsadas', '504-321-53-11', 'выфвфывфывфы', 21231, 1, '2022-12-10 17:35:55', '1', '2kg');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `kind` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `datetime_last_edit` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `title`, `size`, `price`, `description`, `kind`, `gender`, `photo`, `datetime`, `datetime_last_edit`) VALUES
(81, 'Американер з родзинкою', '1kg', 4, '<p><strong>Склад товару:</strong></p><p>борошно пшеничне в/г, цукор білий, маргарин вершковий, яйця курячі, суміш для приготування здобного печива типу \"Американо\", родзинка.</p>', 'Печево', 'Для жінок', '81_6394afd5c1a41', '2022-12-10 19:12:05', '2022-12-10 19:12:05'),
(82, 'Волинське (з арахісом)', '1kg', 3, '<p><strong>Склад товару:</strong></p><p>борошно пшеничне в/г, цукрова пудра, маргарин вершковий, яйця курячі, арахіс смажений подрібнений, сода харчова, крохмаль кукурудзяний, молоко сухе знежирене, ароматизатор Масло-Вершки.</p>', 'Печево', 'На подарунок', '82_6394affb162b7', '2022-12-10 19:12:43', '2022-12-10 19:12:43'),
(83, 'Вишня з фундуком', '2kgMore', 5, '<p><strong>Склад товару:</strong></p><p>глазур кондитерська (вміст: цукор-пісок білий, жир кондитерський, какао-порошок, лецитин, ароматизатор шоколад, ванілін), цукат вишні, фундук смажений подрібнений, цукровий сироп, лимонна кислота.</p>', 'Цукерки', 'Для жінок', '83_6394b03bcdf40', '2022-12-10 19:13:47', '2022-12-10 19:13:47'),
(84, 'Грація в білому шоколаді', '1kg', 1, '<p><strong>Склад товару:</strong></p><p>шоколадна маса ( цукор - 48%, какао- масло - 27,4%, сухе незбиране молоко - 23,7%, емульгатор соєвий лецитин, натуральний ароматизатор \"Ваніль\"), суміш злаків (пластівці вівсяні, пшеничні, кукурудзяні), патока кукурудзяна, сіль.</p>', 'Цукерки', 'На подарунок', '84_6394b21638c7f', '2022-12-10 19:14:36', '2022-12-10 19:21:42'),
(85, 'Грація в чорному шоколаді', '1kg', 2, '<p><strong>Склад товару:</strong></p><p>шоколадна маса (какао терте - 43%, цукор - 43%, какао-масло - 13%, емульгатор соєвий лецитин, натуральний ароматизатор \"Ваніль\"), суміш злаків (пластівці вівсяні, пшеничні, кукурудзяні), патока кукурудзяна, сіль.</p>', 'Цукерки', 'На подарунок', '85_6394b06d7dd6d', '2022-12-10 19:14:37', '2022-12-10 19:14:37'),
(86, 'Гостинний Луцьк', '1kg', 10, '<p><strong>Склад товару:</strong></p><p>Чорнослив з ядром волоського горіха, Чорнослив в глазурі, Курага з ядром волоського горіха, Метеорит соняшниковий, Метеорит арахісовий, Метеорит Волинський, Фінік з ядром волоського горіха, Чорна мантія, Магія ночі, Ностальгія.</p>', 'Солодощі', 'На подарунок', '86_6394b09975d8f', '2022-12-10 19:15:21', '2022-12-10 19:15:21'),
(87, 'До чаю \"Вітаю\"', '1kg', 5, '<p><strong>Склад товару:</strong></p><p>Метеорит соняшниковий, Метеорит арахісовий, Метеорит волинський, Чорна мантія, Магія ночі, Ностальгія.</p>', 'Солодощі', 'Для чоловіків', '87_6394b0be546f1', '2022-12-10 19:15:58', '2022-12-10 19:15:58');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `lastname`, `firstname`, `phone`, `access`) VALUES
(17, 'vlad@123', 'f5bb0c8de146c67b44babbf4e6584cc0', 'Vlad', 'M', '232-323-23-11', 1),
(18, 'test@test', '25d55ad283aa400af464c76d713c07ad', 'test', 'test', '056-232-23-11', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
