<form method="post" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="title" class="form-label">Назва товару</label>
        <input type="text" name="title" class="form-control" id="title" value="<?= $model['title'] ?>">
    </div>
    <?php $size = explode(', ', $model['size'])?>
    <div class="mb-3">
        <h3>Розміри:</h3>
        <input class="form-check-input" type="checkbox" value="1kg" id="1kg"
               name="size[]" <?php if (!empty($size) && in_array('1kg', $size)): echo 'checked="checked"';endif; ?>>
        <label class="form-check-label" for="1kg">
            1kg
        </label>
        <br>
        <input class="form-check-input" type="checkbox" value="2kg" id="2kg"
               name="size[]" <?php if (!empty($size) && in_array('2kg', $size)): echo 'checked="checked"';endif; ?>>
        <label class="form-check-label" for="2kg">
            2kg
        </label>
        <br>
        <input class="form-check-input" type="checkbox" value="2kgMore" id="2kgMore"
               name="size[]" <?php if (!empty($size) && in_array('2kgMore', $size)): echo 'checked="checked"';endif; ?>>
        <label class="form-check-label" for="2kgMore">
            2kgMore
        </label>
        <br>
    </div>
    <div class="mb-3">
        <label for="price" class="form-label">Ціна($)</label>
        <input type="number" name="price" class="form-control" id="price" value="<?= $model['price'] ?>">
    </div>
    <div class="mb-3">
        <label for="description" class="form-label">Опис</label>
        <textarea name="description" class="form-control editor"
                  id="description"><?= $model['description'] ?></textarea>
    </div>
    <div class="mb-3">
        <label for="kind" class="form-label">Категорія товару</label>
        <select id="kind" name="kind">
            <option disabled <?php if (empty($model)): ?> selected<?php endif; ?>>Оберіть категорія товару</option>
            <option value="Солодощі" <?php if ($model['kind'] == 'Солодощі'): ?> selected<?php endif; ?>>Солодощі</option>
            <option value="Печево" <?php if ($model['kind'] == 'Печево'): ?> selected<?php endif; ?>>Печево</option>
            <option value="Цукерки" <?php if ($model['kind'] == 'Цукерки'): ?> selected<?php endif; ?>>Цукерки
            </option>
        </select>
    </div>
    <div class="mb-3">
        <label for="gender" class="form-label">Для кого</label>
        <select id="gender" name="gender">
            <option disabled <?php if (empty($model)): ?> selected<?php endif; ?>>Оберіть для кого товар</option>
            <option value="Для жінок" <?php if ($model['gender'] == 'Для жінок'): ?> selected<?php endif; ?>>Для жінок
            </option>
            <option value="На подарунок" <?php if ($model['gender'] == 'На подарунок'): ?> selected<?php endif; ?>>На подарунок
            </option>
            <option value="Для чоловіків" <?php if ($model['gender'] == 'Для чоловіків'): ?> selected<?php endif; ?>>Для
                чоловіків
            </option>
        </select>
    </div>
    <div class="mb-3">
        <label for="text" class="form-label">Фотографія до товару</label>
        <input type="file" name="file" accept="image/jpeg, image/png" class="form-control" id="file">
    </div>
    <div class="mb-3">
        <? if (is_file('files/products/' . $model['photo'] . '_b.jpg')): ?>
            <img src="/files/news/<?= $model['photo'] ?>_b.jpg"/>
        <? endif; ?>
    </div>
    <button type="submit" class="btn btn-primary">Зберегти</button>
</form>